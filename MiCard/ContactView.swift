//
//  ContactView.swift
//  MiCard
//
//  Created by Leah Joy Ylaya on 12/15/20.
//

import SwiftUI

struct ContactView: View {
    var imageName: String
    var text: String
    var body: some View {
        RoundedRectangle(cornerRadius: 20)
            .fill(Color(.white))
            .frame(height: 40)
            .overlay(HStack {
                Image(systemName: imageName).foregroundColor(Color("miCard-color"))
                Text(text).foregroundColor(Color("miCard-color"))
            })
            .padding(.leading)
            .padding(.trailing)
    }
}
