//
//  ContentView.swift
//  MiCard
//
//  Created by Leah Joy Ylaya on 12/15/20.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        ZStack {
            Color("miCard-color")
                .edgesIgnoringSafeArea(.all)
            VStack {
                
                Image("luna")
                    .resizable()
                    .aspectRatio(contentMode: .fit)
                    .frame(width: 150.0, height: 150.0)
                    .clipShape(Circle())
                    .overlay(
                        Circle().stroke(Color.white, lineWidth: 5)
                )
                Text("Luna Ylaya")
                    .font(Font.custom("Snippet-Regular", size: 40))
                    .bold()
                    .foregroundColor(.white)
                Text("iOS Developer")
                    .foregroundColor(.white)
                    .font(Font.custom("PetitFormalScript-Regular", size: 25))
                Divider()
                ContactView(imageName: "phone.fill", text: "+63 977 346 9496")
                ContactView(imageName: "mail.fill", text: "lunaYlaya@gmail.com")

            }
        }
        
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
