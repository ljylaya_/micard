//
//  MiCardApp.swift
//  MiCard
//
//  Created by Leah Joy Ylaya on 12/15/20.
//

import SwiftUI

@main
struct MiCardApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
